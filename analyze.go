package main

import (
	"io"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

const (
	maxMsgLen          = 150
	njsscanConfig      = ".njsscan"
	reportPath         = "/tmp/njsscan.json"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, projectPath string) (io.ReadCloser, error) {
	removeConfig, err := loadRuleset(projectPath)
	if err != nil {
		return nil, err
	}
	if removeConfig {
		defer os.Remove(filepath.Join(projectPath, njsscanConfig))
	}

	cmd := exec.Command("njsscan", "--json", "--output", reportPath, projectPath)
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)

	if err != nil {
		return nil, err
	}
	return os.Open(reportPath)
}
